Feature : Login to mailbox
Meta:

Narrative:
As a mailbox user
I want to login to mailbox
So that I can get access to mailbox

Scenario: Login into mailbox with existent user
Given Actor is mailbox owner
When Actor login to mailbox
Then Actor's Mailbox page is opened
And List of incoming messages is available