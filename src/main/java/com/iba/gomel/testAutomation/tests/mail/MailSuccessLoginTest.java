package com.iba.gomel.testAutomation.tests.mail;

import com.iba.gomel.testAutomation.lib.feature.common.Account;
import com.iba.gomel.testAutomation.lib.feature.common.AccountBuilder;
import com.iba.gomel.testAutomation.lib.feature.mail.service.MailLoginService;
import org.testng.annotations.Test;

public class MailSuccessLoginTest {

    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to mailbox as a valid user")
    public void successLoginToMailbox() {
        loginService.checkSuccessLogin(account);
    }
}
