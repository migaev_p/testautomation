package com.iba.gomel.testAutomation.tests.mail;

import com.iba.gomel.testAutomation.lib.feature.common.Account;
import com.iba.gomel.testAutomation.lib.feature.common.AccountBuilder;
import com.iba.gomel.testAutomation.lib.feature.mail.service.MailLoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailMissedCredentialLoginTest {

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_NON_EXISTED_ACCOUNT = "";
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getAccountWithWrongPass();

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin(account);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS, "");
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        account = AccountBuilder.getNonExistedAccount();
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin(account);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_NON_EXISTED_ACCOUNT, "");
    }

}
