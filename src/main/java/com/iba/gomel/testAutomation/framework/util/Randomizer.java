package com.iba.gomel.testAutomation.framework.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.UUID;

public class Randomizer {

    public static final int DEFAULT_LENGTH = 20;

    public static String string() {
        return RandomStringUtils.randomAlphanumeric(DEFAULT_LENGTH);
    }

    public static String alphabetic() {
        return RandomStringUtils.randomAlphabetic(DEFAULT_LENGTH);
    }

    public static String numeric() {
        return RandomStringUtils.randomNumeric(DEFAULT_LENGTH);
    }

    public static int number(int startInc, int endExc) {
        return RandomUtils.nextInt(startInc, endExc);
    }

    public byte[] bytes(int count) {
        return RandomUtils.nextBytes(count);
    }

    public UUID uuid() {
        return UUID.randomUUID();
    }
}
