package com.iba.gomel.testAutomation.framework.runner;

import com.iba.gomel.testAutomation.framework.config.GlobalConfig;
import com.iba.gomel.testAutomation.framework.report.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * @author Aleh_Vasilyeu
 */
public class ParallelSuitesListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        Logger.info("SUITE_START : " + suite.getName());
        suite.getXmlSuite().setParallel(GlobalConfig.config().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.config().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
        Logger.info("SUITE_FINISH : " + suite.getName());
    }
}
