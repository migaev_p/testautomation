package com.iba.gomel.testAutomation.lib.feature.steps;

import com.iba.gomel.testAutomation.framework.util.Randomizer;
import com.iba.gomel.testAutomation.lib.feature.common.Account;
import com.iba.gomel.testAutomation.lib.feature.common.AccountBuilder;
import com.iba.gomel.testAutomation.lib.feature.mail.screen.MailLoginPage;
import com.iba.gomel.testAutomation.lib.feature.mail.service.MailLoginService;
import com.iba.gomel.testAutomation.lib.feature.mail.service.MailService;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;


public class MailSteps {

    private Account actor;
    private MailLoginService loginService = new MailLoginService();
    private MailService mailService = new MailService();

    @Given("Actor is mailbox owner")
    public void buildAccount() {
        actor = AccountBuilder.getDefaultAccount();
    }

    @Given("Actor has invalid password")
    public void accountHasInvalidPassword() {
        actor.setPassword(actor.getPassword() + Randomizer.alphabetic());
    }

    @When("Actor open login page")
    public void openLoginPage() {
        MailLoginPage.open();
    }

    @When("Actor enter credentials and click submit")
    @Aliases(values = {"Actor login to mailbox", ""})
    public void login() {
        loginService.loginToMailbox(actor);
    }

    @Then("Actor's Mailbox page is opened")
    public void checkAccountMailboxOpened() {
        Assert.assertTrue(mailService.isAccountMailboxOpened(actor), "Opened mailbox is not actor");
    }

    @Then("List of incoming messages is available")
    public void checkListOfMessages() {
        Assert.assertTrue(mailService.isInboxMessageListIsVisible(), "List on incoming messages not appeared");
    }
}
