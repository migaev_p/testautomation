package com.iba.gomel.testAutomation.lib.feature.mail.screen;

import com.iba.gomel.testAutomation.framework.ui.Browser;
import org.openqa.selenium.By;

import static com.iba.gomel.testAutomation.lib.feature.common.CommonConstants.*;

public class MailLoginPage {

    By loginInput = By.name("login");
    By passInput = By.name("passwd");
    By submitButton = By.cssSelector("button[type='submit']");

    public InboxPage login(String login, String pass) {
        Browser browser = Browser.current();
        browser.writeText(loginInput, login);
        browser.writeText(passInput, pass);
        browser.click(submitButton);
        return new InboxPage();
    }

    public static MailLoginPage open() {
        Browser browser = Browser.rise();
        browser.open(MAILBOX_URL);
        MailLoginPage mailLoginPage = new MailLoginPage();
        browser.waitForAppear(mailLoginPage.loginInput);
        return mailLoginPage;
    }


}
