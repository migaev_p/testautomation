package com.iba.gomel.testAutomation.lib.feature.mail.screen;

public class MailboxMenu {

    // Locators

    public InboxPage openInbox() {
        return new InboxPage();
    }

    public SentPage openSent() {
        return new SentPage();
    }

    public DeletedPage openDeleted() {
        return new DeletedPage();
    }

    public SpamPage openSpam() {
        return new SpamPage();
    }

    public DraftsPage openDrafts() {
        return new DraftsPage();
    }

}
