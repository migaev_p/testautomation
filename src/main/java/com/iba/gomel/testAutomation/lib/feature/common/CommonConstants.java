package com.iba.gomel.testAutomation.lib.feature.common;

public interface CommonConstants {
    String MAILBOX_URL = "http://mail.yandex.ru";

    String DEFAULT_USER_LOGIN = "tat-test-user@yandex.ru";
    String DEFAULT_USER_PASSWORD = "tat-123qwe";
    String DEFAULT_USER_MAIL = "tat-test-user@yandex.ru";

    String DEFAULT_MAIL_TO_SEND = DEFAULT_USER_MAIL;
}
