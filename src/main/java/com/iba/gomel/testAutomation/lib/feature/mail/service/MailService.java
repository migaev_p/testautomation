package com.iba.gomel.testAutomation.lib.feature.mail.service;

import com.iba.gomel.testAutomation.lib.feature.common.Account;
import com.iba.gomel.testAutomation.lib.feature.mail.Letter;
import com.iba.gomel.testAutomation.framework.report.Logger;

public class MailService {

    public void sendLetter(Account account, Letter letter) {
        Logger.debug("Send letter: " + letter.toString());
        if(letter.containsAttach()) {
            letter.getAttachment().getAbsolutePath();
        }
    }

    public boolean isLetterExistInInbox(Account account, Letter letter) {
        Logger.debug("Check is letter exists in inbox: " + letter.toString());
        return false;
    }

    public boolean isAccountMailboxOpened(Account actor) {
        return false;
    }

    public boolean isInboxMessageListIsVisible() {
        return false;
    }

}
