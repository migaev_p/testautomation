package com.iba.gomel.testAutomation.lib.feature.common;


import com.iba.gomel.testAutomation.framework.util.Randomizer;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(CommonConstants.DEFAULT_USER_LOGIN);
        account.setPassword(CommonConstants.DEFAULT_USER_PASSWORD);
        account.setEmail(CommonConstants.DEFAULT_USER_MAIL);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + Randomizer.numeric());
        return account;
    }

    public static Account getNonExistedAccount() {
        Account account = getDefaultAccount();
        account.setLogin(account.getLogin() + Randomizer.numeric());
        return account;
    }

}
