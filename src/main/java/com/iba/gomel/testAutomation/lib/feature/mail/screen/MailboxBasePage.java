package com.iba.gomel.testAutomation.lib.feature.mail.screen;

import com.iba.gomel.testAutomation.framework.ui.Browser;
import org.openqa.selenium.By;

public abstract class MailboxBasePage {

    private By composeLetterLink = By.xpath("");

    public MailboxMenu menu() {
        // check if exists
        this.menu().openInbox().getCurrentAddress();
        return new MailboxMenu();
    }

    public WriteLetterPage openWriteLetter() {
        Browser.current().click(composeLetterLink);
        return new WriteLetterPage();
    }

    public InboxPage refresh() {
        // do some check for verify that page has refreshed
        return null;
    }

}
