package com.iba.gomel.testAutomation.lib.feature.mail.service;

import com.iba.gomel.testAutomation.lib.feature.mail.screen.LoginFailedPage;
import com.iba.gomel.testAutomation.lib.feature.common.Account;
import com.iba.gomel.testAutomation.lib.feature.mail.screen.MailLoginPage;
import com.iba.gomel.testAutomation.framework.report.Logger;

public class MailLoginService {

    public void loginToMailbox(Account account) {
        Logger.info("Login to mailbox with account: " + account);
        MailLoginPage.open().login(account.getLogin(), account.getPassword());
    }

    public void checkSuccessLogin(Account account) {
        Logger.info("Check login is successful under account: " + account);
        String address = MailLoginPage.open()
                .login(account.getLogin(), account.getPassword())
                .getCurrentAddress();
        if (account == null || !address.equals(account.getEmail())) {
            throw new RuntimeException("Login to mailbox failed. Login = " + account.getLogin() + ", Password = " + account.getPassword() + ". Current email = " + address);
        }
    }

    public String retrieveErrorOnFailedLogin(Account account) {
        Logger.info("Get error message on failed login for account: " + account);
        MailLoginPage.open().login(account.getLogin(), account.getPassword());
        return new LoginFailedPage().getErrorMassage();
    }


}
