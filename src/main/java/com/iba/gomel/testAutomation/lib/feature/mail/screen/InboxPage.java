package com.iba.gomel.testAutomation.lib.feature.mail.screen;

import com.iba.gomel.testAutomation.framework.ui.Browser;
import org.openqa.selenium.By;

public class InboxPage extends MailboxBasePage {
    private By currentAddressElement = By.cssSelector("#nb-1");

    public String getCurrentAddress() {
        return Browser.current().getText(currentAddressElement).trim();

    }
}
