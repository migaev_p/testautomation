package com.iba.gomel.testAutomation.lib.feature.mail.screen;

import com.iba.gomel.testAutomation.framework.ui.Browser;
import org.openqa.selenium.By;

public class LoginFailedPage {

    By errorMessage = By.className("");

    public String getErrorMassage() {
        return Browser.current().getText(errorMessage);
    }
}
